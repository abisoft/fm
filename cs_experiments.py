import tensorflow as tf
import datetime,subprocess
from kb import KBSampler

from utils import DataTypes,powerset
from trainer import Trainer


class FModel:
    def __init__(self, kb, k_hidden, factor_reg,bias_reg,init_std,eval_every_epochs,data_folder):
        self.kb = kb
        self.k_hidden = k_hidden
        self.total_num_features = self.kb.num_features
        self.MAX_FEATURE_LENGTH = self.kb.max_feature_length
        self.FACTOR_REG = factor_reg
        self.BIAS_REG = bias_reg
        self.init_std = init_std
        self.eval_every_epochs =eval_every_epochs
        self.data_folder = data_folder
        self.__create_graph()

    def reset(self):
        if hasattr(self, 'session'):
            self.session.close()
        self.session = tf.Session()
        # self.trained = False

    def __create_graph(self):
        """MODEL"""

        self.feature_indices_positives = tf.placeholder(tf.int32, shape=[None,self.MAX_FEATURE_LENGTH])
        self.feature_indices_negatives = tf.placeholder(tf.int32, shape=[None,self.MAX_FEATURE_LENGTH])

        self.feature_values_positives = tf.placeholder(tf.float32, shape=[None,self.MAX_FEATURE_LENGTH])
        self.feature_values_negatives = tf.placeholder(tf.float32, shape=[None,self.MAX_FEATURE_LENGTH])
        self.batch_size = tf.placeholder(tf.int32)


        # with tf.device('/cpu:0'):
        self.parameter_matrix = tf.Variable(tf.random_uniform([self.total_num_features+1, self.k_hidden + 1], -self.init_std, self.init_std)) # 1 for the biases
        self.feature_embeddings = tf.slice(self.parameter_matrix, [0,1], [self.total_num_features + 1, self.k_hidden]) #
        self.biases = tf.reshape(tf.slice(self.parameter_matrix, [0,0], [self.total_num_features + 1, 1]), [self.total_num_features + 1]) #

        self.factors_positives = tf.gather(self.feature_embeddings,self.feature_indices_positives)
        self.factors_negatives = tf.gather(self.feature_embeddings,self.feature_indices_negatives)

        self.biases_positives = tf.gather(self.biases,self.feature_indices_positives)
        self.biases_negatives = tf.gather(self.biases,self.feature_indices_negatives)

        self.feature_vals_positives = tf.reshape(self.feature_values_positives,[self.batch_size,self.MAX_FEATURE_LENGTH,1])
        self.feature_vals_negatives = tf.reshape(self.feature_values_negatives,[self.batch_size,self.MAX_FEATURE_LENGTH,1])

        self.factor_sum_positives = tf.reduce_sum(self.factors_positives * self.feature_vals_positives, 1)
        self.factor_sum_negatives = tf.reduce_sum(self.factors_negatives * self.feature_vals_negatives, 1)

        self.predictions_positives = 0.5 * tf.reduce_sum(self.factor_sum_positives * self.factor_sum_positives, [1]) \
                                     - 0.5 * tf.reduce_sum(self.feature_vals_positives * self.feature_vals_positives * self.factors_positives * self.factors_positives,[1,2])\
                                     + tf.reduce_sum(self.biases_positives * self.feature_values_positives,[1])

        self.regularization_positives = 0.5 * (self.FACTOR_REG * tf.reduce_sum(self.factors_positives * self.factors_positives)
                                              + self.BIAS_REG * tf.reduce_sum(self.biases_positives * self.biases_positives))


        self.predictions_negatives = 0.5 * tf.reduce_sum(self.factor_sum_negatives * self.factor_sum_negatives, [1]) \
                                     - 0.5 * tf.reduce_sum(self.feature_vals_negatives * self.feature_vals_negatives * self.factors_negatives * self.factors_negatives, [1,2]) \
                                     + tf.reduce_sum(self.biases_negatives * self.feature_values_negatives, [1])

        self.regularization_negatives = 0.5 * (self.FACTOR_REG * tf.reduce_sum(self.factors_negatives * self.factors_negatives)
                                               + self.BIAS_REG * tf.reduce_sum(self.biases_negatives * self.biases_negatives))


        self.regularization = self.regularization_positives + self.regularization_negatives
        #BPR loss
        self.loss = tf.reduce_sum(tf.nn.softplus(tf.subtract(self.predictions_negatives,
                                                                 self.predictions_positives))) + self.regularization



        self.loss = tf.Print(self.loss,[self.loss],"Loss is : ")
        tf.summary.scalar('loss',self.loss)
        self.summary_op = tf.summary.merge_all()

    def train(self, session=tf.Session(), learning_rate=0.1, max_epochs=200, pos_per_batch=1024,
              log_folder=None, optimizer_type=None, output_path=""):

        config = tf.ConfigProto()
        config.gpu_options.allow_growth=True
        session = tf.Session(config=config)

        batcher = KBSampler(self.kb, pos_per_batch=pos_per_batch,data_types={DataTypes.TRAIN_LABEL})
        if optimizer_type == 'Adam':
            optimizer = tf.train.AdamOptimizer(learning_rate)
        else:
            optimizer = tf.train.AdagradOptimizer(learning_rate)

        placeholders = [self.feature_indices_positives, self.feature_values_positives,
                        self.feature_indices_negatives, self.feature_values_negatives, self.batch_size]

        self.session = session

        if log_folder is not None:
            log_folder = log_folder + "/" + datetime.datetime.now().strftime("%Y-%m-%d-%H-%M")
        summary_writer = tf.train.SummaryWriter(log_folder)

        # optimize
        trainer = Trainer(optimizer, max_epochs)
        trainer(batcher, placeholders=placeholders, model=self,loss=self.loss,summary_op=self.summary_op,session=self.session,summary_writer=summary_writer)

    def test2file(self,predictfile):
        test_facts = self.kb.get_all_facts(data_types={DataTypes.TEST_LABEL})
        temp_output = []
        batches = KBSampler(self.kb, 512,1, [DataTypes.TEST_LABEL], False)
        for batch,pos_facts in batches:
            feed_dict = {self.feature_indices_positives: batch[0], self.feature_values_positives: batch[1],self.batch_size: batch[-1]}
            scores = self.session.run([self.predictions_positives], feed_dict=feed_dict)[0]
            temp_output.extend(zip(scores, pos_facts))
        testresults = sorted(temp_output, key=lambda x: x[0], reverse=True)
        with open(predictfile, 'w', encoding="utf8") as fh:
            for score, fact in testresults:
                fh.write(str(score) + "\t" + fact.triple[1][0] + "\t" + fact.triple[1][
                    1] + "\t" + "REL$NA" + "\t" + fact.triple[0] + "\n")


    def getMAP(self,filename):
        predictfile = (self.data_folder+"/predict_{}_.txt".format(filename))
        self.test2file(predictfile)
        output_folder = os.path.dirname(predictfile)
        # cmd = 'cd %s ; scala eval.scala %s ' % (BASE_FOLDER, predictfile)
        cmd = 'scala eval.scala %s ' % (predictfile)
        process = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
        proc_stdout = process.communicate()[0].strip()
        tablefile = os.path.join(output_folder, "table.txt".format(filename))
        results_file = os.path.join(output_folder, "results_{}_.txt".format(filename))
        with open(results_file,encoding="utf-8",mode="a") as results_fh:
            with open(tablefile,encoding="utf-8") as fID:
                results = []
                for line in fID:
                    if line.startswith('Average'):
                        results.append(line.split()[-2])
                    if line.startswith('Global'):
                        results.append(line.split()[-2])
                print(*results,sep="\t",file=results_fh)
        # os.remove(predictfile)

    def dotprod(self, indices_r, indices_e):
        dotprod = self.session.run(self.dotprod_pos, feed_dict={self.indices_e_pos: indices_e, self.indices_r_pos: indices_r})
        return dotprod

    def sigma_dotprod(self, indices_r, indices_e):
        sigma_dotprod = self.session.run(self.sigma_dotprod_pos,feed_dict={self.indices_e_pos: indices_e, self.indices_r_pos: indices_r})
        return sigma_dotprod

    def load_model(self, session):
        saver = tf.train.Saver()
        model_save_dir = config["SYSTEM"]["model_save_dir"] + "/"
        checkpoint = tf.train.get_checkpoint_state(model_save_dir)
        if checkpoint and checkpoint.model_checkpoint_path:
            saver.restore(session, checkpoint.model_checkpoint_path)
        self.session = session
        return session





if __name__ == "__main__":
    SEED = 4
    import numpy as np
    import tensorflow as tf
    np.random.seed(SEED)
    tf.set_random_seed(SEED)
    import os,datetime,json

    from cs_reader import Reader
    from kb import KB

    FRACTION = 1.0
    RANK = 100
    BATCH_SIZE = 1024
    INIT_STD = 0.1
    LEARNING_RATE = 0.0001
    FACTOR_REG = 0.01
    BIAS_REG = 0.01
    EVAL_EVERY_EPOCHS = 10
    NUM_EPOCHS = 1000

    OPTIMIZER_TYPE = "Adam"
    SAVE_DIR = "./models"
    LOG_DIR = "./tmp/logs"

    KB_FILE = os.path.join(SAVE_DIR, "fm_%03d.pckl" % (int(100 * FRACTION)))
    DATA_FILE = "./data/datafile_new_full.txt"

    feature_path = "./data/results/{}".format("FMC")
    thresholds = np.linspace(0,0.5,21)
    for thresh in thresholds:
        FB_FRACTION = thresh  # fraction of freebase training data to use
        RESULTS_FOLDER = os.path.join(feature_path,datetime.datetime.now().strftime("%Y-%m-%d-%H-%M-%S"))

        config = {"SEED":SEED,"FRACTION":FRACTION,"FB_FRACTION":FB_FRACTION,"RANK":RANK,"BATCH_SIZE":BATCH_SIZE,"INIT_STD":INIT_STD,
                  "FACTOR_REG":FACTOR_REG,"BIAS_REG":BIAS_REG,"OPTIMIZER_TYPE":OPTIMIZER_TYPE,"LEARNING_RATE":LEARNING_RATE,
                  }
        if not os.path.exists(RESULTS_FOLDER):
            os.makedirs(RESULTS_FOLDER)
            with open(os.path.join(RESULTS_FOLDER,"config.json"),encoding="utf-8",mode="w") as fh:
                json.dump(config,fh,indent=4,sort_keys=True)

        reader = Reader(DATA_FILE)
        reader.read(FRACTION,FB_FRACTION)
        KB_TRAIN = reader.create_kb() #
        model = FModel(KB_TRAIN,RANK,FACTOR_REG,BIAS_REG,INIT_STD,EVAL_EVERY_EPOCHS,RESULTS_FOLDER)
        model.train(learning_rate=LEARNING_RATE, max_epochs=NUM_EPOCHS, pos_per_batch=BATCH_SIZE, optimizer_type=OPTIMIZER_TYPE,log_folder=LOG_DIR,output_path=SAVE_DIR)
        model.getMAP(FB_FRACTION)
        tf.reset_default_graph()

