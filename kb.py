import numpy as np
import scipy.sparse as sp
import pickle, itertools

from collections import defaultdict, OrderedDict
from ordered_set import OrderedSet
from settings import config
from utils import Fact, FactTypes, DataTypes, Dimensions


class KB:
    def __init__(self):
        self.__facts = OrderedDict()
        self.__relations = OrderedSet()
        self.__entitypairs = OrderedSet()
        self.__entities = OrderedSet()
        self.__dimensions = Dimensions(0, 0, 0)
        self.__relation_facts = defaultdict(set)
        self.__entitypair_facts = defaultdict(set)
        self.num_features = 0
        self.entitypair_features = {}
        self.max_feature_length = 0

    def __len__(self):
        return len(self.__facts)

    def __add_to_facts(self, fact):
        key = (fact.triple, fact.truth, fact.data_type)
        self.__facts[key] = fact

    def __remove_from_facts(self, key):
        del self.__facts[key]

    def __add_relation(self, relation):
        if not relation in self.__relations:
            self.__relations.add(relation)
            self.__dimensions.num_relations += 1

    def __add_entitypair(self, entitypair):
        (entity1, entity2) = entitypair
        if not entitypair in self.__entitypairs:
            self.__entitypairs.add(entitypair)
            self.__dimensions.num_entitypairs += 1
        if not entity1 in self.__entities:
            self.__entities.add(entity1)
            self.__dimensions.num_entities += 1
        if not entity2 in self.__entities:
            self.__entities.add(entity2)
            self.__dimensions.num_entities += 1

    def __add_to_relation_facts(self, relation, fact):
        if not relation in self.__relation_facts:
            self.__relation_facts.update({relation: {fact}})
        else:
            self.__relation_facts[relation].add(fact)

    def __add_to_entitypair_facts(self, entitypair, fact):
        if not entitypair in self.__entitypair_facts:
            self.__entitypair_facts.update({entitypair: {fact}})
        else:
            self.__entitypair_facts[entitypair].add(fact)

    def get_all_facts(self, data_types=set([])):
        if len(data_types) == 0:
            return self.__facts.values()
        else:
            return set([fact for (key, fact) in self.__facts.items() if fact.data_type in data_types])

    def get_corpus_entitypairs(self):
        return set([fact for (key, fact) in self.__facts.items() if fact.fact_type == FactTypes.TEXTUAL_FACT])

    def add(self, fact):
        assert len(fact.triple) == 2
        (rel, tup) = fact.triple
        truth = fact.truth
        assert isinstance(truth, bool)
        typ = fact.data_type
        key = ((rel, tup), truth, typ)
        if not self.contains_fact(key):
            self.__add_to_facts(fact)
            self.__add_relation(rel)
            self.__add_entitypair(tup)
            self.__add_to_relation_facts(rel, fact)
            self.__add_to_entitypair_facts(tup, fact)

    def contains_fact(self, key):
        return key in self.__facts  # check if exact fact already in kb

    def contains_fact_any_type(self, triple,
                               truth):  # check if this relation and entity tuple with same truth label already in kb, for any type
        return self.contains_fact((triple, truth, DataTypes.TRAIN_LABEL)) or \
               self.contains_fact((triple, truth, DataTypes.DEV_LABEL)) or \
               self.contains_fact((triple, truth, DataTypes.TEST_LABEL)) or \
               self.contains_fact((triple, truth, DataTypes.TMP_LABEL))

    def add_train(self, triple):
        fact = Fact(triple, True, DataTypes.TRAIN_LABEL, "")
        self.add(fact)

    def add_train_stochastic(self, test_prob, triple):
        # add true fact with given prob to test data, otherwise to training data
        if np.random.uniform(0, 1.0) >= test_prob:
            self.add(Fact(triple, True, DataTypes.TRAIN_LABEL, ""))
        else:
            self.add(Fact(triple, True, DataTypes.TEST_LABEL, ""))

    def get_facts_relation(self, relation):
        result = set()
        if relation in self.__relations:
            result = self.__relation_facts[relation]
        return result

    def get_facts_entitypair(self, entitypair):
        result = set()
        if entitypair in self.__entitypairs:
            result = self.__entitypair_facts[entitypair]
        return result

    def is_true(self, key):
        return key in self.__facts

    def sample_neg_given_relation(self, relation, oracle=False, tries=100):
        tup = self.__entitypairs[np.random.randint(0, self.__dimensions.num_entitypairs)]
        triple = (relation, tup)
        cell = (triple, True, DataTypes.TRAIN_LABEL)
        if tries == 0:
            print("Warning, couldn't sample negative fact for ", relation)
            return cell
        elif (triple, True, DataTypes.TRAIN_LABEL) in self.__facts:
            return self.sample_neg_given_relation(relation, oracle, tries - 1)
        else:
            return cell

    def pseudo_sample_neg_given_relation(self, relation, oracle=False):
        tup = self.__entitypairs[np.random.randint(0, self.__dimensions.num_entitypairs)]
        cell = (relation, tup)
        return cell

    def get_entitypairs(self):
        return self.__entitypairs

    def get_entitypair_ids(self):
        return self.__entitypairs.map

    def get_relations(self):
        return self.__relations

    def get_relation_ids(self):
        return self.__relations.map

    def get_relation_index(self, relation):
        return self.__relations.index(relation)

    def get_dims(self):
        value = [self.__dimensions.num_relations, self.__dimensions.num_entitypairs]
        return value

    def to_data_frame(self):
        data = {}
        for rel in self.__relations:
            row = list()
            for tup in self.__entitypairs:
                if ((rel, tup), True, DataTypes.TRAIN_LABEL) in self.__facts:
                    row.append(1.0)
                elif ((rel, tup), False, DataTypes.TRAIN_LABEL) in self.__facts:
                    row.append(0.0)
                elif ((rel, tup), True, DataTypes.TEST_LABEL) in self.__facts:
                    row.append("*1")
                elif ((rel, tup), False, DataTypes.TEST_LABEL) in self.__facts:
                    row.append("*0")
                elif ((rel, tup), True, DataTypes.TMP_LABEL) in self.__facts:
                    row.append("**1")
                elif ((rel, tup), True, DataTypes.TMP_LABEL) in self.__facts:
                    row.append("**0")
                else:
                    row.append("")
            data[rel] = row
        import pandas as pd
        df = pd.DataFrame(data, index=self.__entitypairs)  # , index=(self.__rels,self.__tups))
        return df

    def dump_pckl(self, kbfile):
        with open(kbfile, 'wb') as fID:
            pickle.dump(self, fID)

    @staticmethod
    def load_pckl(kbfile):
        try:
            with open(kbfile, 'rb') as fID:
                kb = pickle.load(fID)
            return kb
        except:
            return None


class KBSampler:
    BATCH_RANDOM = True  # True: randomly select indices to create batch; False: linearly go through training data

    def __init__(self, kb, pos_per_batch=4, neg_per_pos=1, data_types={DataTypes.TRAIN_LABEL},
                 batch_random=BATCH_RANDOM):

        self.kb = kb  # object of type KB

        # make sure only positive facts are considered
        self.pos_facts = [f for f in kb.get_all_facts(data_types=data_types) if f.truth]  # (make sure only True facts)

        self.num_pos_facts = len(self.pos_facts)

        if pos_per_batch == -1:
            self.pos_per_batch = self.num_pos_facts
        else:
            self.pos_per_batch = min(max(1, pos_per_batch), self.num_pos_facts)

        self.neg_per_pos = max(1, neg_per_pos)

        self.batch_random = batch_random
        self.pos_seen_current_epoch = 0
        self.epoch = 1

        """default: no fact filtering"""
        self.filtered_fact_indices = list(range(self.num_pos_facts))

    def __reset(self):
        self.pos_seen_current_epoch = 0
        if self.batch_random:
            np.random.shuffle(self.filtered_fact_indices)

    def __iter__(self):
        return self

    def __next__(self):
        if self.pos_seen_current_epoch >= len(self.filtered_fact_indices):  # self.num_pos_facts:
            self.epoch += 1
            self.__reset()
            raise StopIteration
        return self.get_batch()

    def get_batch(self):

        if self.batch_random:
            sample_filtered_indices = np.random.choice(len(self.filtered_fact_indices), size=self.pos_per_batch,
                                                       replace=True)
        else:
            # linearly go thru data +ve samples
            sample_filtered_indices = np.arange(self.pos_seen_current_epoch,
                                                min(self.pos_seen_current_epoch + self.pos_per_batch,
                                                    len(self.filtered_fact_indices)))

        pos_fact_ind = [self.filtered_fact_indices[i] for i in sample_filtered_indices]
        pos_facts_sample = [self.pos_facts[i] for i in pos_fact_ind]

        pos_rel = [f.triple[0] for f in pos_facts_sample]
        pos_tup = [f.triple[1] for f in pos_facts_sample]

        if self.neg_per_pos == 1:
            neg_facts_sample = [self.kb.sample_neg_given_relation(f.triple[0]) for f in pos_facts_sample]
        else:
            neg_facts_sample = [[self.kb.sample_neg_given_relation(f.triple[0]) for i in range(self.neg_per_pos)] for f
                                in
                                pos_facts_sample]
            neg_facts_sample = list(itertools.chain.from_iterable(neg_facts_sample))  # flatten

        self.pos_seen_current_epoch += self.pos_per_batch

        pos_row_indices = []
        pos_col_indices = []
        pos_facts_values = []
        pos_row_index = 0

        feature_indices_positives = []
        feature_values_positives = []
        for fact in pos_facts_sample:
            index = 0
            feature_indices = np.zeros(self.kb.max_feature_length)
            feature_values = np.zeros(self.kb.max_feature_length)
            (relation, entity_pair) = fact.triple
            relation_index = self.kb.get_relation_index(relation)
            features = fact.props["tuple_features"]

            feature_indices[index] = int(relation_index) + 1
            feature_values[index] = 1

            index += 1

            for (feature_ind, feature_val) in features:
                feature_indices[index] = feature_ind + 1
                feature_values[index] = feature_val
                index += 1
            feature_indices_positives.append(feature_indices)
            feature_values_positives.append(feature_values)

        feature_indices_negatives = []
        feature_values_negatives = []
        for fact in neg_facts_sample:
            index = 0
            feature_indices = np.zeros(self.kb.max_feature_length)
            feature_values = np.zeros(self.kb.max_feature_length)
            (relation, entity_pair) = fact[0]
            relation_index = self.kb.get_relation_index(relation)
            features = self.kb.entitypair_features[entity_pair]

            feature_indices[index] = int(relation_index) + 1
            feature_values[index] = 1

            index += 1

            for (feature_ind, feature_val) in features:
                feature_indices[index] = feature_ind + 1
                feature_values[index] = feature_val
                index += 1
            feature_indices_negatives.append(feature_indices)
            feature_values_negatives.append(feature_values)

        feature_indices_positives, feature_indices_negatives = np.array(feature_indices_positives,
                                                                        dtype=np.int32), np.array(
            feature_indices_negatives, dtype=np.int32)
        feature_values_positives, feature_values_negatives = np.array(feature_values_positives,
                                                                      dtype=np.float64), np.array(
            feature_values_negatives, dtype=np.float64)

        batch_size = len(feature_indices_positives)
        assert len(feature_indices_positives) == len(feature_values_positives)
        assert len(feature_indices_negatives) == len(feature_values_negatives)
        return [feature_indices_positives, feature_values_positives, feature_indices_negatives,
                feature_values_negatives, batch_size], pos_facts_sample

    def get_epoch(self):
        return self.epoch
