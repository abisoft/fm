
import re
import numpy as np
from time import time
from _decimal import Decimal



from kb import KB
from utils import Fact,DataTypes
from settings import config


DATAFILE = "./data/datafile_new_full.txt"

class Reader():
    def __init__(self,datafile=DATAFILE):
        self.facts = {}
        self.max_feature_id = -1
        self.tup_features = {}
        self.DATAFILE = datafile


    def read(self,data_fraction,fb_fraction):
        #Read data
        with open(self.DATAFILE,encoding="utf-8") as fh:
            for line in fh:
                random_uniform = np.random.uniform(0, 1.0)
                if len(line.strip()) > 0 and random_uniform < data_fraction:
                    relation, e1, e2, type_, truth,features = line.strip().split('\t')
                    data_type = {'Train': DataTypes.TRAIN_LABEL, 'Test': DataTypes.TEST_LABEL}[type_]
                    truth = True
                    props = dict({})
                    tuple_features = self.get_tup_features(features.strip().split("|||"))

                    self.tup_features[(e1, e2)] = tuple_features

                    if (type_ == 'Train' and relation.startswith("REL$")):
                        if not random_uniform < fb_fraction:
                            continue
                    props["tuple_features"] = tuple_features
                    fact = Fact((relation,(e1,e2)),truth,data_type, props)
                    self.facts[fact.key] = fact


    def get_numfeatures(self):
        return self.max_feature_id+1 #

    def get_tup_features(self,features):
        tup_features = []
        for feature in features:
            feature_ind,feature_val = feature.strip().split(":")
            feature_ind = int(feature_ind.strip())
            feature_val = Decimal(feature_val.strip())
            tup_features.append((feature_ind,feature_val))
            if feature_ind > self.max_feature_id:
                self.max_feature_id = feature_ind
        return tup_features


    def create_kb(self,data_types={DataTypes.TRAIN_LABEL, DataTypes.TEST_LABEL},truthiness={True,False}):
        self.kb = KB()
        self.kb.num_features = self.get_numfeatures()
        self.kb.entitypair_features = self.tup_features
        self.kb.max_feature_length = len(max(self.tup_features.values(),key=len)) + 1 #  add 1 for relations
        print("Max num of features is ",self.kb.num_features)
        for (triple,truth,data_type),fact in self.facts.items():
            if data_type in data_types and truth in truthiness:
                self.kb.add(fact)

        return self.kb



if __name__ == "__main__":
    FRACTION,FB_FRACTION = 1.0,1.0
    DATAFILE = "datafile.txt"
    t1 = time()
    reader = Reader(DATAFILE)
    reader.read(FRACTION,FB_FRACTION)
    kb = reader.create_kb(data_types={DataTypes.TEST_LABEL})
    pos_facts = [f for f in kb.get_all_facts() if f.truth]
    print(len(pos_facts))
