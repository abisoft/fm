"""
adapted from naga.shared.trainer
"""

import tensorflow as tf
from time import time
from tqdm import tqdm

class Trainer(object):
    """
    Object representing a TensorFlow trainer.
    """

    def __init__(self, optimizer, max_epochs):
        self.loss = None
        self.optimizer = optimizer
        self.max_epochs = max_epochs

    #@profile
    def __call__(self, batcher, placeholders, loss, summary_op,model=None, session=None,summary_writer=None):
        self.loss = loss
        self.summary_writer = summary_writer
        self.model = model
        minimization_op = self.optimizer.minimize(loss)
        close_session_after_training = False
        if session is None:
            session = tf.Session()
            close_session_after_training = True  # no session existed before, we provide a temporary session

        # init = tf.initialize_all_variables()
        init = tf.global_variables_initializer()
        session.run(init)
        
        

        epoch = 1
        iteration = 1

        for epoch in tqdm(range(self.max_epochs), unit='epoch'):
            epochtime = 0.
            optimizetime = 0.
            t0=time()
            for values,_ in batcher:
                feed_dict = {}
                for i in range(0, len(placeholders)):
                    feed_dict[placeholders[i]] = values[i]
                t1=time()
                _, current_loss,summary_string = session.run([minimization_op, loss,summary_op], feed_dict=feed_dict)
                optimizetime += (time()-t1)
                
                # for hook in self.hooks:
                #     hook(session, epoch, iteration, model, current_loss)
                self.summary_writer.add_summary(summary_string,iteration)
                self.summary_writer.flush()
                iteration += 1

            # calling post-epoch hooks
            # for hook in self.hooks:
            #     hook(session, epoch, 0, model, 0.)
            epochtime = time()-t0
#            print('time on optimization in this epoch',optimizetime)
#            print('total time of this epoch ',epochtime)

            if (epoch+1) % self.model.eval_every_epochs == 0:
                self.model.getMAP(epoch)
            epoch += 1

        if close_session_after_training:
            session.close()
