import tensorflow as tf
import numpy as np
from enum import Enum
from collections import defaultdict
import itertools,math
from itertools import combinations_with_replacement,takewhile,count,chain,combinations


class Fact(object):
    # triple has form (relation,(entity_one,entity_two))
    # props is other properties of fact eg doc_info, etc
    # Truth is boolean - True, False
    def __init__(self,triple,truth,data_type,props):
        self.triple = triple
        self.truth = truth
        self.data_type = data_type
        self.props = props
        self.key = (self.triple,self.truth,self.data_type)


class Dimensions(object):
    def __init__(self,num_entitypairs,num_relations,num_entities):
        self.num_entitypairs = num_entitypairs
        self.num_relations = num_relations
        self.num_entities = num_entities

class FactTypes(Enum):
    KNOWLEDGEBASE_FACT = 1
    TEXTUAL_FACT = 2


class DataTypes(Enum):
    TRAIN_LABEL = 1
    DEV_LABEL = 2
    TEST_LABEL = 3
    TMP_LABEL = 4

def sigmoid(x):
    return 1 / (1 + np.exp(-x))



def loss_logistic(outputs, y):
    margins = -y * outputs
    raw_loss = tf.log(tf.add(1.0, tf.exp(margins)))
    return tf.minimum(raw_loss, 100, name='truncated_log_loss')


def loss_mse(outputs, y):
    return tf.pow(y -  outputs, 2, name='mse_loss')



def loss_bpr(outputs_positives, outputs_negatives):
    diff_scores = tf.subtract(outputs_negatives,outputs_positives)
    return tf.nn.softplus(diff_scores)

def loss_logistic_(outputs_positives, outputs_negatives):
    return tf.nn.softplus(-outputs_positives)+tf.nn.softplus(outputs_negatives)






def batcher(X_, y_=None, batch_size=-1):
    """Split data to mini-batches.

    Parameters
    ----------
    X_ : {numpy.array, scipy.sparse.csr_matrix}, shape (n_samples, n_features)
        Training vector, where n_samples in the number of samples and
        n_features is the number of features.

    y_ : np.array or None, shape (n_samples,)
        Target vector relative to X.

    batch_size : int
        Size of batches.
        Use -1 for full-size batches

    Yields
    -------
    ret_x : {numpy.array, scipy.sparse.csr_matrix}, shape (batch_size, n_features)
        Same type as input
    ret_y : np.array or None, shape (batch_size,)
    """
    n_samples = X_.shape[0]

    if batch_size == -1:
        batch_size = n_samples
    if batch_size < 1:
       raise ValueError('Parameter batch_size={} is unsupported'.format(batch_size))

    for i in range(0, n_samples, batch_size):
        upper_bound = min(i + batch_size, n_samples)
        ret_x = X_[i:upper_bound]
        ret_y = None
        if y_ is not None:
            ret_y = y_[i:i + batch_size]
        yield (ret_x, ret_y)


def batch_to_feeddict(batch, core):
    (pos_facts_sparse, pos_row_indices, pos_col_indices, neg_facts_sparse, neg_row_indices, neg_col_indices) = batch

    fd = {}

    # sparse case
    fd[core.row_indices_positive] = pos_row_indices
    fd[core.col_indices_positive] = pos_col_indices
    fd[core.row_indices_negative] = neg_row_indices
    fd[core.col_indices_negative] = neg_col_indices

    fd[core.raw_values_positives] = pos_facts_sparse.data.astype(np.float32)
    fd[core.raw_shape_positives] = np.array(pos_facts_sparse.shape).astype(np.int64)

    fd[core.raw_values_negatives] = neg_facts_sparse.data.astype(np.float32)
    fd[core.raw_shape_negatives] = np.array(neg_facts_sparse.shape).astype(np.int64)


    indices = np.concatenate((np.concatenate((pos_row_indices[:,np.newaxis],pos_col_indices[:,np.newaxis]), axis=1),
                             np.concatenate((neg_row_indices[:,np.newaxis],neg_col_indices[:,np.newaxis]), axis=1)),axis=0)


    if not len(indices) == len(pos_facts_sparse.data.astype(np.float32)) + len(neg_facts_sparse.data.astype(np.float32)):
        import pdb
        pdb.set_trace()
    return fd

# from docs.python.org
def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s) + 1))

