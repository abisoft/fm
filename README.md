# fm

Factorization Machines

 The factorization machines implementation used for experiments in the paper Contextual Pattern Embeddings for One-shot Relation Extraction


Requires tensorflow 0.12.1 and Python 3.5


The dataset used can be found at https://drive.google.com/file/d/1ZSAX9qVTNwRo-5lFh1jlpWacQv22rQJh/view?usp=sharing

Run with `python cs_experiments.py`


Results are saved in data/results